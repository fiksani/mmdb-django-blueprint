from __future__ import unicode_literals

from django.db import models


class MovieDetails(models.Model):
    title = models.CharField(max_length=500)
    description = models.TextField()
    stars = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name_plural = 'Movie Details'
        
    def __str__(self):
        return self.title


class MovieReview(models.Model):
    movie = models.ForeignKey(MovieDetails, related_name='reviews')
    user_name = models.CharField(max_length=100)
    review = models.TextField()
